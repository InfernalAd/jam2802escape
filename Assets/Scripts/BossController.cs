﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour {
	Vector2 speed = new Vector2(0,0);
	float maxSpeed = 15.0f;
	float acceleration = 10.0f;
	float rotationSpeed = 5.0f;
	public GameObject player;
	public GameObject bulletPrefab;
	bool isShooting = false;
	
	[SerializeField]
	[Range(0.01f,5.0f)]
	private float reloadTime = 0.5f;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		//HandleInput();
		
		HandleAI();
	}

	void HandleAI ()
	{
		speed = gameObject.rigidbody2D.velocity;
		Vector2 direction = player.transform.position - transform.position;
		/**/
		if (direction.y > 0.4)
		{
			if (direction.magnitude < 0.3)
				return;
			
			speed += new Vector2(0,1) * acceleration*Time.deltaTime;
			if (speed.magnitude > maxSpeed)
			{
				speed = speed.normalized*maxSpeed;
			}
		}
		else
		{
			if (direction.magnitude < 0.3)
				return;
			speed += direction.normalized * acceleration*Time.deltaTime;
			if (speed.magnitude > maxSpeed)
			{
				speed = speed.normalized*maxSpeed;
			}
		}
		
		
		Vector2 dir = player.transform.position - transform.position;
		Vector3 dir3 = new Vector3 (dir.x,dir.y,0);
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg-90;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		
		RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position,dir3);
		
		if (hits.Length > 0)
		{
			Debug.DrawLine (transform.position, new Vector3(hits[0].point.x,hits[0].point.y,0));
		}
		//GameObject.Instantiate(bulletPrefab,transform.position + dir3.normalized * 0.1f,transform.rotation);
		foreach(RaycastHit2D hit in hits)
		{
			if (hit.collider != null && hit.collider.tag == "Player")
			{
				if (!isShooting)
				{
					isShooting = true;
					Invoke("Shoot",reloadTime);
				}
			}
		}
		
		gameObject.rigidbody2D.velocity = speed;
	}

	void Shoot ()
	{
		
		Vector2 dir = player.transform.position - transform.position;
		Vector3 dir3 = new Vector3 (dir.x, dir.y, 0);
		GameObject bullet = (GameObject)GameObject.Instantiate (bulletPrefab, transform.position + dir3.normalized * 0.15f, transform.rotation);
		bullet.rigidbody2D.velocity = dir.normalized*5;
		bullet.rigidbody2D.velocity += gameObject.rigidbody2D.velocity;
		isShooting = false;
	}
	
	void HandleInput()
	{
		speed = rigidbody2D.velocity;
		bool wasInput = false;
		if (Input.GetKey(KeyCode.W))
		{
			speed += new Vector2(0,acceleration*Time.deltaTime);
			wasInput = true;
		}
		if (Input.GetKey(KeyCode.S))
		{
			speed -= new Vector2(0,acceleration*Time.deltaTime);
			wasInput = true;
		}
		if (Input.GetKey(KeyCode.A))
		{
			speed -= new Vector2(acceleration*Time.deltaTime,0);
			wasInput = true;
		}
		if (Input.GetKey(KeyCode.D))
		{
			speed += new Vector2(acceleration*Time.deltaTime,0);
			wasInput = true;
		}
		if (wasInput)
		{
			if (speed.magnitude > maxSpeed)
			{
				speed = speed.normalized*maxSpeed;
			}
			gameObject.rigidbody2D.velocity = speed;
		}	
	}
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		WallConroller wallController = collision.gameObject.GetComponent<WallConroller>();
		if (wallController != null)
		{
			wallController.SetBroken(true);
		}
	}
}
