﻿using UnityEngine;
using System.Collections;

public class WallConroller : MonoBehaviour {
	[SerializeField]
	private bool isBroken = false;
	[SerializeField]
	private Sprite normalSprite;
	[SerializeField]
	private Sprite brokenSprite;
	
	private SpriteRenderer spriteRenderer;
	private BoxCollider2D collider;

	// Use this for initialization
	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		collider = gameObject.GetComponent<BoxCollider2D>();
	}
	
	public void SetBroken (bool b)
	{
		isBroken = b;
		if (!isBroken)
		{
			spriteRenderer.sprite = normalSprite;
			collider2D.enabled = true;
		}
		else
		{
			
			spriteRenderer.sprite = brokenSprite;
			collider2D.enabled = false;
		}
	}
	
	
}
