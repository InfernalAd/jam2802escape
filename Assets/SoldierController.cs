﻿using UnityEngine;
using System.Collections;

public class SoldierController : MonoBehaviour {
	
	private GameObject player;

	bool isShooting = false;
	[SerializeField]
	private GameObject bulletPrefab;
	[SerializeField]
	[Range(0.01f,5.0f)]
	private float reloadTime = 1.0f;

	bool isDead = false;

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Hero");
	}
	
	// Update is called once per frame
	void Update () {
		HandleAI();
	}

	void HandleAI ()
	{
		if (isDead) return;	
		Vector2 dir = player.transform.position - transform.position;
		Vector3 dir3 = new Vector3 (dir.x,dir.y,0);
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg-90;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position,dir3);
		
		if (hits.Length > 0)
		{
			Debug.DrawLine (transform.position, new Vector3(hits[0].point.x,hits[0].point.y,0));
		}
		foreach(RaycastHit2D hit in hits)
		{
			if (hit.collider != null && hit.collider.tag == "Player")
			{
				if (!isShooting)
				{
					isShooting = true;
					Invoke("Shoot",reloadTime);
				}
			}
		}
	}
	
	void Shoot ()
	{
		GetComponent<Animator>().SetTrigger("TimeToShoot");
		Vector2 dir = player.transform.position - transform.position;
		Vector3 dir3 = new Vector3 (dir.x, dir.y, 0);
		GameObject bullet = (GameObject)GameObject.Instantiate (bulletPrefab, transform.position + dir3.normalized * 0.15f, transform.rotation);
		bullet.rigidbody2D.velocity = dir.normalized*5;
		bullet.rigidbody2D.velocity += gameObject.rigidbody2D.velocity;
		isShooting = false;
	}
	
	void Die ()
	{
		collider2D.enabled = false;
		isDead = true;
		GetComponent<Animator>().SetTrigger("TimeToDie");
	}
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.transform.tag == "Bullet")
			Die();
	}
}
