﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SegmentController : MonoBehaviour {
	[SerializeField]
	private GameObject[] segmentPrefabs;
	[SerializeField]
	private List<GameObject> segments = new List<GameObject>();

	[SerializeField]
	private GameObject soldierPrefab;

	float segmentHeight = 0.48f;
	
	public Random random = new Random();
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		PopulateSegments();
		List<GameObject> segmentsToRemove = new List<GameObject>();
		foreach (GameObject segment in segments)
		{
			if (!isInScreen(segment))
			{
				GameObject.Destroy(segment);
				segmentsToRemove.Add(segment);
			}
		}
		foreach (GameObject segment in segmentsToRemove)
		{
			segments.Remove(segment);
		}
		
	}

	void PopulateSegments ()
	{
		if (CalculateMaxY() < Screen.height)
		{
			
			Vector3 highestSegmentPos = GetHighestSegmentPos();
			highestSegmentPos += new Vector3(0,segmentHeight,0);
			int prefabIndex = Random.Range(0,segmentPrefabs.Length);
			GameObject prefab = segmentPrefabs[prefabIndex];
			GameObject instantiatedPrefab = (GameObject)GameObject.Instantiate (prefab);
			instantiatedPrefab.transform.position = highestSegmentPos;
			segments.Add (instantiatedPrefab);
			if (Random.Range(0,3) == 0)
			{
				GameObject instantiatedSoldier = (GameObject)GameObject.Instantiate (soldierPrefab);
				instantiatedSoldier.transform.position = highestSegmentPos;
				int randomYIndex = Random.Range(0,1);
				if (randomYIndex == 0)
				{
					instantiatedSoldier.transform.position += new Vector3(0,segmentHeight/2,0);
				} 
				else
				{
					instantiatedSoldier.transform.position += new Vector3(0,-segmentHeight/2,0);
				}
				int randomXIndex = Random.Range(0,4);
				instantiatedSoldier.transform.position += new Vector3(randomXIndex*0.2f,0,0);
			}
		}
	}
	
	float CalculateMaxY()
	{
		float maxY = float.MinValue;
		foreach (GameObject segment in segments)
		{
			if (Camera.main.WorldToScreenPoint(segment.transform.position).y > maxY)
			{
				maxY = Camera.main.WorldToScreenPoint(segment.transform.position).y;
			}
		}
		return maxY;
	}
	
	Vector3 GetHighestSegmentPos()
	{
		float minY = float.MinValue;
		GameObject hightestSegment = null;
		foreach (GameObject segment in segments)
		{
			if (hightestSegment == null)
				hightestSegment = segment;
			if (Camera.main.WorldToScreenPoint(segment.transform.position).y > minY)
			{
				minY = Camera.main.WorldToScreenPoint(segment.transform.position).y;
				hightestSegment = segment;
			}
		}
		if (hightestSegment != null)
			return hightestSegment.transform.position;
		return new Vector3(0,-segmentHeight*2,0);
	}

	bool isInScreen (GameObject segment)
	{
		Vector3 screenPos = Camera.main.WorldToScreenPoint(segment.transform.position);
		return (screenPos.x < Screen.width && screenPos.x >= 0 && screenPos.y < Screen.height*2 && screenPos.y >= -Screen.height);
	}
}
