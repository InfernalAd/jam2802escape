﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	private Vector2 speed = new Vector2(0,0);
	private float maxSpeed = 15.0f;
	private float acceleration = 15.0f;
	private float initialMaxSpeed = 0;
	private float initialAcceleration = 0;
	private bool isSprinting = false;
	private bool isDead = false;
	[SerializeField]
	[Range(0.01f,5.0f)]
	private float reloadTime = 0.5f;
	
	[SerializeField]
	private GameObject bulletPrefab;

	bool isShooting = false;	

	// Use this for initialization
	void Start () {
		initialMaxSpeed = maxSpeed;
		initialAcceleration = acceleration;
	}
	
	// Update is called once per frame
	void Update () {
		HandleInput();
		HandleGamepadInput();
		
		if (isDead) return;
		
	}
	
	void HandleInput()
	{
		if (isDead) return;
		speed = rigidbody2D.velocity;
		bool wasInput = false;
		if (Input.GetKey(KeyCode.W))
		{
			speed += new Vector2(0,acceleration*Time.deltaTime);
			wasInput = true;
		}
		if (Input.GetKey(KeyCode.S))
		{
			speed -= new Vector2(0,acceleration*Time.deltaTime);
			wasInput = true;
		}
		if (Input.GetKey(KeyCode.A))
		{
			speed -= new Vector2(acceleration*Time.deltaTime,0);
			wasInput = true;
		}
		if (Input.GetKey(KeyCode.D))
		{
			speed += new Vector2(acceleration*Time.deltaTime,0);
			wasInput = true;
		}
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			maxSpeed = initialMaxSpeed*2;
			acceleration = initialAcceleration*2;
			isSprinting = true;
		}
		if (Input.GetKeyUp(KeyCode.LeftShift))
		{
			maxSpeed = initialMaxSpeed;
			acceleration = initialAcceleration;
			isSprinting = false;
		}
		if (Input.GetMouseButtonDown(0))
		{
			Shoot();
		}
		if (Input.GetKeyDown(KeyCode.RightBracket))
		{
			Die();
		}
		if (wasInput)
		{
			rigidbody2D.drag = 10;
			if (speed.magnitude > maxSpeed)
			{
				speed = speed.normalized*maxSpeed;
			}
			gameObject.rigidbody2D.velocity = speed;
		}
		else
		{
			rigidbody2D.drag = 100;	
		}
		Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		
		Vector2 dir = mouseWorldPosition - transform.position;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg-90;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}

	void HandleGamepadInput ()
	{
	}
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		/*if (collision.relativeVelocity.magnitude < 1) return;
		
		WallConroller wallController = collision.gameObject.GetComponent<WallConroller>();
		if (wallController != null)
		{
			wallController.SetBroken(true);
			Invoke("DisableSprint",0.1f);
		}*/
	}
	
	void DisableSprint()
	{
		isSprinting = false;
		maxSpeed = initialMaxSpeed;
		acceleration = initialAcceleration;
	}

	void Shoot ()
	{
		if (!isShooting)
		{
			GetComponent<Animator>().SetTrigger("TimeToShoot");
			Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 dir3 = mouseWorldPosition - transform.position;
			GameObject bullet = (GameObject)GameObject.Instantiate (bulletPrefab, transform.position + dir3.normalized * 0.15f, transform.rotation);
			bullet.rigidbody2D.velocity = dir3.normalized*5;
			bullet.rigidbody2D.velocity += gameObject.rigidbody2D.velocity;
			isShooting = true;
			Invoke("ResetShoot",reloadTime);
		}
			
	}
	
	void Die ()
	{
		isDead = true;
		GetComponent<Animator>().SetTrigger("TimeToDie");
	}
	
	void ResetShoot ()
	{
		isShooting = false;
	}
}
