﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {
	[SerializeField]
	private GameObject impact;
	
	// Use this for initialization
	void Start () {
		Invoke("Destroy",5);
	}
	
	// Update is called once per frame
	void OnCollisionEnter2D () {
		GetComponent<SpriteRenderer>().enabled = false;
		impact.SetActive(true);
		collider2D.enabled = false;
		rigidbody2D.isKinematic = true;
		Invoke("Destroy",0.15f);
	}
	
	void Destroy()
	{
		GameObject.Destroy(gameObject);
	}
}
